# Python Todo

App demenstrating how to create a RESTful API in Python Django

## Setup
clone the repository

Create the virtual environment and activate it.
```shell
virtualenv .venv
source ./.venv/bin/activate
```
install all dependencies

```shell
pip install djangorestframework django
```
run migrations
```shell
PYTHONPATH=. DJANGO_SETTINGS_MODULE=todo_app.settings django-admin migrate
```

## Optional

Create a super user
```shell
python3 manage.py createsuperuser
```
## Running
```shell
PYTHONPATH=. DJANGO_SETTINGS_MODULE=todo_app.settings django-admin runserver
```
