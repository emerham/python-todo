from django.urls import path, include

from rest_framework.routers import DefaultRouter

from todo import views

router = DefaultRouter()
router.register(r"todo", views.TaskViewSet)
router.register(r"users", views.UserViewSet)

# The API URLs are now determined
urlpatterns = [
    path('', include(router.urls))
]
