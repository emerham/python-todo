from django.db import models

# Create your models here.


class Task(models.Model):
    STATES = (("todo", "Backlog"),
              ("wip", "Work in Progress"),
              ("done", "Done"))
    title = models.CharField(max_length=255, blank=False, unique=True)
    description = models.TextField()
    owner = models.ForeignKey(
        'auth.User', related_name='task', on_delete=models.CASCADE)
    status = models.CharField(max_length=4, choices=STATES, default="todo")
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
