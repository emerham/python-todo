from rest_framework import serializers
from todo.models import Task
from django.contrib.auth.models import User


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Task
        fields = ('url', 'id', 'title', 'description',
                  'status', 'owner', 'updated', 'created')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    task = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name='task-detail')

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'email', 'task')
